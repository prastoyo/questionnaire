<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $table = 'detail_survey';

      // menyimpan data tanpa timestamps(created_at, updated_at, delete_at)
      // public $timestamps = false;
  
      protected $fillable = ['id', 'id_user', 'id_question','id_answer'];
}
