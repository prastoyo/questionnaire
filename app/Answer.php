<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'answer';

  // menyimpan data tanpa timestamps(created_at, updated_at, delete_at)
    public $timestamps = false;

    protected $fillable = ['id', 'answer'];
}
