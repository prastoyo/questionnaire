<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'list_question';

  // menyimpan data tanpa timestamps(created_at, updated_at, delete_at)
    public $timestamps = false;

    protected $fillable = ['id', 'question'];
}
