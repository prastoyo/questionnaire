<?php

namespace App\Http\Controllers;
use App\Survey;
use App\User;
use App\Question;
use App\Answer;
use App\Exports\SurveyExport;
use Illuminate\Http\Request;
use Carbon\Carbon;

class SurveyController extends Controller
{
    public function export(){
        $dt = Carbon::today()->toDateString();

        return (new SurveyExport)->download('export kuisioner.xlsx');
    }

    public function show(){
        $dt = Carbon::today()->toDateString();
        $data1 = Question::get();
        $data2 = Answer::get();

        if (count(Survey::get()) != 0) {
        $answer1 = Survey::where([['id_answer', '=',  '1'],['created_at' , 'like', '%'.$dt.'%']])->count();
        $answer2 = Survey::where([['id_answer', '=',  '2'],['created_at' , 'like', '%'.$dt.'%']])->count();
        $answer3 = Survey::where([['id_answer', '=',  '3'],['created_at' , 'like', '%'.$dt.'%']])->count();
        $answer4 = Survey::where([['id_answer', '=',  '4'],['created_at' , 'like', '%'.$dt.'%']])->count();
        $total = Survey::where('created_at' , 'like', '%'.$dt.'%')->count();
        $percent1 = $answer1/$total*100;
        $percent2 = $answer2/$total*100;
        $percent3 = $answer3/$total*100;
        $percent4 = $answer4/$total*100;
        }else{
            $percent1 = 'belum ada';
            $percent2 = 'belum ada';
            $percent3 = 'belum ada';
            $percent4 = 'belum ada';
        }
        return view('welcome',compact('data1','data2','percent1','percent2','percent3','percent4'));
        
    }

    public function insertSurvey(Request $request){
        User::create([
            'name' => $request->name,
            'school' => $request->school,
        ]);

        $data1 = User::orderBy('id', 'desc')->first();
        $count = 1;
            $data2 = Question::get();
            foreach($data2 as $d2){
                Survey::create([
                    'id_user' => $data1->id,
                    'id_answer' => $request->answer[$count++],
                    'id_question' => $d2['id']
                ]); 
            }
            
        

        return redirect()->back();
    }
}
