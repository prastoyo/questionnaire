<?php

namespace App\Exports;

use App\Survey;
use App\User;
use App\Question;
use App\Answer;
use Carbon\Carbon;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class SurveyExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    
    public function view(): View
    {
        $dt = Carbon::today()->toDateString();
        $user = User::where('created_at' , 'like', '%'.$dt.'%')->get();
        $countquestion = Question::get()->count();
        foreach($user as $u){
            $u['percentanswer1'] = Survey::where([['id_answer', '=',  '1'],['id_user', '=',  $u['id']],['created_at' , 'like', '%'.$dt.'%']])->count() / $countquestion;
            $u['percentanswer2'] = Survey::where([['id_answer', '=',  '2'],['id_user', '=',  $u['id']],['created_at' , 'like', '%'.$dt.'%']])->count() / $countquestion;
            $u['percentanswer3'] = Survey::where([['id_answer', '=',  '3'],['id_user', '=',  $u['id']],['created_at' , 'like', '%'.$dt.'%']])->count() / $countquestion;
            $u['percentanswer4'] = Survey::where([['id_answer', '=',  '4'],['id_user', '=',  $u['id']],['created_at' , 'like', '%'.$dt.'%']])->count() / $countquestion;
        }
        return view('excel.survey', compact('user'));
    }
}
