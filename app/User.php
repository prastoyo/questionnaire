<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'user';

  // menyimpan data tanpa timestamps(created_at, updated_at, delete_at)
    // public $timestamps = false;

    protected $fillable = ['id', 'name', 'school'];
}
