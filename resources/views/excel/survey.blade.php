<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Asal Sekolah</th>
            <th>Tidak Sangat Puas</th>
            <th>Tidak Puas</th>
            <th>Puas</th>
            <th>Sangat Puas</th>
        </tr>
    </thead>
    <tbody>
    @php $no = 1 @endphp
    @foreach($user as $u)
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $u->name }}</td>
            <td>{{ $u->school }}</td>
            <td>{{ $u->percentanswer1 }}%</td>
            <td>{{ $u->percentanswer2 }}%</td>
            <td>{{ $u->percentanswer3 }}%</td>
            <td>{{ $u->percentanswer4 }}%</td>
        </tr>
    @endforeach
    </tbody>
</table>