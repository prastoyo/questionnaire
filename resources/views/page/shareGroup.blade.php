<!-- Modal -->
<div class="modal fade" id="modalShareGroup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Share Folder Group</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <div class="form-group">
          <form action="{{ url('file/sharegroup') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <label>Apakah anda akan share file / folder ini?</label>
            <label id="modal-myvar">Alamat Share : </label>
            <div class="input-group col-xs-12">
              <input type="hidden" name="id" id="id-upload">
              <select class="form-control" id="exampleSelectGender">
              @foreach($data2 as $items2)
                          <option>{{ $items2->role }}</option>
              @endforeach
              </select>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>