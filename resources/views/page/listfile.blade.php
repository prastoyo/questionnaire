
@if(!session()->has('username'))
  <script>window.location = "{{url('dashboard/')}}";</script>
@endif

@include('template.header')



<!-- partial -->
<div class="container-fluid page-body-wrapper">
  <!-- partial:partials/_sidebar.html -->
  @include('template.sidebar')
  <!-- partial -->
  <div class="main-panel">
    <div class="content-wrapper">

      <!-- Button trigger modal -->



      <div class="row">
        <div class="col-md-12 grid-margin">
          <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
              <div class="mr-md-3 mr-xl-5">
                <h2>Halaman List File</h2>
              </div>

            </div>
            <div class="d-flex justify-content-between align-items-end flex-wrap">

              <button class="btn btn-primary mr-3 mt-2 mt-xl-0" data-toggle="modal" data-target="#modalInputFile">Tambah File</button>
              <button class="btn btn-primary mr-3 mt-2 mt-xl-0" data-toggle="modal" data-target="#modalInputFolder">Tambah Folder</button>

            </div>
          </div>
        </div>
      </div>

      @include('page.inputFile')
      @include('page.inputFolder')




      <div class="row">
        <div class="col-md-12 stretch-card">
          <div class="card">
            <div class="card-body">
              <p class="card-title">Data File</p>
              <div class="table-responsive">
                <table id="data" class="table">
                  <thead>
                    <tr>
                      <th>Nama File</th>
                      <th>User</th>
                      <th>Waktu</th>
                      <th>Link</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if(!empty($status))
                    <tr>
                      <td>
                        <a href="{{ url('listfile/'.urlencode($backlocation )) }}"> . . . </a>
                      </td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    @endif
                    @foreach($data as $items)
                    <tr>
                      @if( $items->type == 'folder')
                      <td>
                        <a href="{{ url('listfile/'.urlencode($items->locationurl)) }}"> {{ $items->NAME }}</a>
                      </td>
                      @else
                      <td>
                        {{ $items->NAME }}
                      </td>
                      @endif
                      <td>{{ $items->EMAIL }}</td>
                      <td>{{ $items->TIMERS }}</td>
                      <td>{{ $items->public_share }}</td>
                      <td>
                        <div class="btn-group">
                          <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown">Aksi</button>
                          <div class="dropdown-menu">
                            <a data-toggle="modal" data-target="#modalDelete" data-id="{{$items->id_files}}" class="dropdown-item delete-modal">Hapus</a>
                            <a class="dropdown-item share-modal" data-toggle="modal" data-target="#modalShare" data-id="{{$items->public_share}}" data-id2="{{$items->id_files}}">Share Publik</a>
                            <a class="dropdown-item share-group-modal" data-toggle="modal" data-target="#modalShareGroup" data-id="{{$items->id_files}}">Share Grup</a>
                          </div>
                        </div>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->
    <!-- partial:partials/_footer.html -->
    @include('template.footer')
    <!-- partial -->
  </div>
  <!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

@include('page.delete')
@include('page.share')
@include('page.shareGroup')

<!-- plugins:js -->
<script src="{{url('vendors/base/vendor.bundle.base.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="{{url('vendors/chart.js/Chart.min.js')}}"></script>
<script src="{{url('vendors/datatables.net/jquery.dataTables.js')}}"></script>
<script src="{{url('vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{url('js/off-canvas.js')}}"></script>
<script src="{{url('js/hoverable-collapse.js')}}"></script>
<script src="{{url('js/template.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{url('js/dashboard.js')}}"></script>
<script src="{{url('js/data-table.js')}}"></script>
<script src="{{url('js/jquery.dataTables.js')}}"></script>
<script src="{{url('js/dataTables.bootstrap4.js')}}"></script>
<script src="{{url('js/file-upload.js')}}"></script>

<script>
  $(document).ready(function() {
    $('#data').DataTable();

    //     $('#myModal').on('shown.bs.modal', function () {
    //   $('#myInput').trigger('focus')
    // })

    var clipboard = new ClipboardJS('.btn');
    clipboard.on('success', function(e) {
      alert("Link Sudah Dicopy")

    });

  });
</script>
<!-- End custom js for this page-->
<script>
  $(document).on("click", ".share-modal", function() {
    var sharelink = $(this).data('id')
    var id = $(this).data('id2');
    $(".modal-body #link-share").val(sharelink);
    $(".modal-body #id-upload").val(id);
  });
</script>

<script>
  $(document).on("click", ".hapus-modal", function() {
    var id = $(this).data('id');
    $(".modal-body #id-upload").val(id);
  });
</script>

<script>
  $(document).on("click", ".group-share-modal", function() {
    var id = $(this).data('id');
    $(".modal-body #id-upload").val(id);
  });
</script>


</body>

</html>