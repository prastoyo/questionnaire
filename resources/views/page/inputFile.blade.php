<!-- Modal -->
<div class="modal fade" id="modalInputFile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Input File</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <div class="form-group">
          <form action="{{ url('file/store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <label>Pilih File Yang Akan Diupload</label>
            <span id="modal-myvalue"></span> <span id="modal-myvar"></span> <span id="modal-bb"></span>
            <input type="hidden" value="{{ $location }}" name="location">
            <input type="file" name="file" class="file-upload-default">
            <div class="input-group col-xs-12">
              <input name="file" type="text" class="form-control file-upload-info" disabled="" placeholder="Upload File">
              <span class="input-group-append">
                <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
              </span>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>