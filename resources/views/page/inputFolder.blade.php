<!-- Modal -->
<div class="modal fade" id="modalInputFolder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Input File</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <div class="form-group">
          <form action="{{ url('file/storefolder') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <label>Pilih File Yang Akan Diupload</label>
            <label>Lokasi : {{ $location }}</label>
            <input type="hidden" value="{{ $location }}" name="location">
            <div class="form-group">
                    <label>Nama Folder</label>
                    <input type="text" name="name" class="form-control" placeholder="Nama Folder" >
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>