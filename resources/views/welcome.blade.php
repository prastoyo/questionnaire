@include('template.header')



<!-- partial -->
<div class="container-fluid page-body-wrapper">
  <!-- partial:partials/_sidebar.html -->
  @include('template.sidebar')
  <!-- partial -->
  <div class="main-panel">
    <div class="content-wrapper">

      <!-- Button trigger modal -->



      <div class="row">
        <div class="col-md-12 grid-margin">
          <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
              <div class="mr-md-3 mr-xl-5">
                <h2>Silahkan Mengisi Kuisioner</h2>
              </div>

            </div>
            <div class="d-flex justify-content-between align-items-end flex-wrap">

            

            </div>
          </div>
        </div>
      </div>





      <div class="row">
        <div class="col-md-12 stretch-card">
          <div class="card">
            <div class="card-body">
              <form action="{{ url('/insert') }}" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
                <div class="form-group">
                  <label for="exampleInputUsername1">Nama</label>
                  <input name="name" type="text" class="form-control" id="exampleInputUsername1" placeholder="Nama">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Asal Sekolah</label>
                  <input name="school" type="text" class="form-control" id="exampleInputEmail1" placeholder="Asal Sekolah">
                </div>



                <div class="table-responsive">
                  <table id="data" class="table">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Pertanyaan</th>
                        <th>Sangat Tidak Puas</th>
                        <th>Tidak Puas</th>
                        <th>Puas</th>
                        <th>Sangat Puas</th>
                      </tr>
                    </thead>
                    <tbody>
                    @php($count=1)
                      @foreach($data1 as $items1)
                      <tr>
                        <td>{{ $count++ }}</td>
                        <td>
                          {{ $items1->question }}
                        </td>
                        @foreach($data2 as $items2)
                        <td><input name="answer[{{ $count-1 }}]" type="radio" value="{{ $items2->id }}"></td>
                        @endforeach
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>

              <div class="table-responsive">
                  <table id="data" class="table">
                    <thead>
                      <tr>
                        <th>Sangat Tidak Puas</th>
                        <th>Tidak Puas</th>
                        <th>Puas</th>
                        <th>Sangat Puas</th>
                      </tr>
                    </thead>
                    <tbody>
                   
                      <tr>
                        <td>{{ $percent1 }}</td>
                        <td>
                          {{ $percent2 }}
                        </td>
                        <td>
                          {{ $percent3 }}
                        </td>
                        <td>
                          {{ $percent4 }}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>

            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->
    <!-- partial:partials/_footer.html -->
    @include('template.footer')
    <!-- partial -->
  </div>
  <!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->


<!-- plugins:js -->
<script src="{{url('vendors/base/vendor.bundle.base.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="{{url('vendors/chart.js/Chart.min.js')}}"></script>
<script src="{{url('vendors/datatables.net/jquery.dataTables.js')}}"></script>
<script src="{{url('vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{url('js/off-canvas.js')}}"></script>
<script src="{{url('js/hoverable-collapse.js')}}"></script>
<script src="{{url('js/template.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{url('js/dashboard.js')}}"></script>
<script src="{{url('js/data-table.js')}}"></script>
<script src="{{url('js/jquery.dataTables.js')}}"></script>
<script src="{{url('js/dataTables.bootstrap4.js')}}"></script>
<script src="{{url('js/file-upload.js')}}"></script>

<script>
  $(document).ready(function() {
    $('#data').DataTable();

    //     $('#myModal').on('shown.bs.modal', function () {
    //   $('#myInput').trigger('focus')
    // })



  });
</script>
<!-- End custom js for this page-->

</body>

</html>